'use strict';

class SubmitForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {input: '', words: [] };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  getData = text => {
    fetch("http://127.0.0.1:5000/wordify?text=" + text, {
      method: 'POST'
    }).then(response => {
      return response.json();
    }).then(data => {
      this.setState({words: data});
    }).catch(err => console.error(err));
  };

  handleChange(event) {
    this.setState({input: event.target.value});
  }

  handleSubmit(event) {
    this.getData(this.state.input);
    event.preventDefault();
  }

  render() {
    return (
      <div>
        <form className="box" onSubmit={this.handleSubmit}>
          <div className="field">
            <div className="control">
              <textarea className="textarea" maxlength="6000" placeholder="Paste text in here" rows="10" value={this.state.input} onChange={this.handleChange} ></textarea>
            </div>
          </div>
          <div className="field">
            <div className="control has-text-centered">
              <button className="button is-link">Submit</button>
          </div>
          </div>
        </form>
        {this.state.words.length > 0 && 
          <div className="box">
            <table className="table is-fullwidth is-hoverable">
              <thead>
                <tr>
                  <th>Word</th>
                  <th>Occurrences</th>
                  <th>Rarity (per million)</th>
                </tr>
              </thead>
              <tbody>
                {
                  this.state.words.map(w => {
                    return <tr key={ w.word }><td>{ w.word }</td><td>{ w.count }</td><td>{ w.perMillion }</td></tr>;
                  })
                }
              </tbody>
            </table>
          </div>
        }
      </div>
    );
  }
}

const domContainer = document.querySelector('#app');
ReactDOM.render(React.createElement(SubmitForm), domContainer);