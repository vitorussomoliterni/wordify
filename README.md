## Wordify ##

App that makes simple text analysys: it takes the 10 most used words from an input text and runs them through a word analytics API to find how frequently they are used in the English language.  
Try it here: [https://vitorm.pythonanywhere.com/](https://vitorm.pythonanywhere.com/).

Frontend in React, backend in python (flask).

To run locally the flask app needs to run on port 5000, and it requires an API key for [WordsApi](https://rapidapi.com/wordsapi/api/wordsapi), setting it as an enviromental variable called WORDSAPIKEY.