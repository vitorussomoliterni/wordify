from flask import Flask, request, Response, jsonify
from flask_cors import CORS
import json, string, collections, requests, os

app = Flask(__name__)
CORS(app)
apiKey = os.environ.get("WORDSAPIKEY", '')

# Splits text into a list getting rid of whitespace and punctuation
def getWordsList(text):
  words = text.split()
  words = [word.lower() for word in words]
  # For simplicity's sake I'll just ignore all words with less than 3 characters
  filteredWordsList = list(filter(lambda word: len(word) > 2, words))
  table = str.maketrans('', '', string.punctuation)
  result = [w.translate(table) for w in filteredWordsList]
  return result

# Orders the words list based on their occurrence
def getMostUsedWords(wordsList):
  counter = collections.Counter(wordsList)
  return counter.most_common()

# Uses Words API to get some stats (just their frequency in the English language, for now)
def getWordsStats(wordsList):
  result = []
  for word in wordsList:
    try:
      response = requests.get("https://wordsapiv1.p.mashape.com/words/" + word[0] + "/frequency", headers={ "X-Mashape-Key": apiKey })
    except:
      continue

    responseText = json.loads(response.text)

    # Skips word if not found in the API
    if 'frequency' not in responseText:
      continue
    
    perMillion = responseText['frequency']['perMillion']
    wordDict = dict([
      ('word', word[0]),
      ('count', word[1]),
      ('perMillion', perMillion)
    ])
    result.append(wordDict)
    response.close()

  return result

# POST /wordify?text
@app.route('/wordify', methods=['POST'])
def wordify():
  text = request.args.get('text')
  wordList = []

  if text:
    wordList = getWordsList(text)
  else:
    return Response(status=400)

  orderedList = getMostUsedWords(wordList)
  frequencyList = getWordsStats(orderedList[:10]) # Let's take the top 10 words, for now

  return json.dumps(frequencyList)

if __name__ == '__main__':
  app.run()